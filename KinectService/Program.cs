﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartHomeCore.Networking;
using KinectService.Kinect;
using System.Windows.Media.Imaging;
using System.IO;

namespace KinectService {
    public class KinectServiceProgram {
        static void Main(string[] args) {
            new System.Threading.Thread(ServerMain).Start();
            System.Threading.Thread.Sleep(2000);
            ClientMain();
            return;
        }

        static void ClientMain() {
            KinectClient kc = new KinectClient("localhost", 32000);
            kc.FrameArrived += kc_FrameArrived;
            kc.Start();
            // Must start the connection and then enable streams.
            kc.EnableRGBStream(true, 30).EnablePointCloudStream(true, 30);
            // After setup, reset all kinects
            kc.ResetBaseTime();
            System.Threading.Thread.Sleep(20000);
            kc.Stop();
        }

        static void kc_FrameArrived(object sender, SmartHomeCore.Kinect.KinectClientFrameArrivedEventArgs e) {
            Console.WriteLine("Frame arrived Type: " + e.frameType);
            double timestamp;
            string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            if (e.frameType == SmartHomeCore.Kinect.FrameType.RGB_Data) {
                BitmapFrame frame = e.source_interface.getDecodedImage(out timestamp);
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(frame);
                string path = (myPhotos + @"\test.jpg");
                using (FileStream fs = new FileStream(path, FileMode.Create)) {
                    encoder.Save(fs);
                }
            }
            if (e.frameType == SmartHomeCore.Kinect.FrameType.Point_cloud_data) {
                float[,] point_cloud = e.source_interface.getDeflatedPointCloud(out timestamp);
                using (FileStream fs = new FileStream(myPhotos + @"\test.txt", FileMode.Create)) {
                    BinaryWriter bw = new BinaryWriter(fs);
                    for (int i = 0; i < point_cloud.GetLength(0); i++) {
                        bw.Write(point_cloud[i, 0] + "," + point_cloud[i, 1] + "," + point_cloud[i, 2] + System.Environment.NewLine);
                    }
                }
            }
            // You get the idea!
        }

        public static void ServerMain() {
            KinectNode node = new KinectNode();
            Console.WriteLine("Initializing Kinect");
            node.Open();
            System.Threading.Thread.Sleep(2000);
            KinectServer ks = new KinectServer(node, 32000);
            ks.Start();
            System.Threading.Thread.Sleep(int.MaxValue);
            ks.Stop();
            node.Close();
        }
    }
}
