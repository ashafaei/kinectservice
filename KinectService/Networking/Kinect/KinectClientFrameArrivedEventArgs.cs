﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using SmartHomeCore.Networking.Kinect;

namespace SmartHomeCore.Kinect {
    public enum FrameType {
        RGB_Data, BodyIndex_Data, Depth_data, Point_cloud_data, ir_data, body_data, EOE
    }

    [ProtoContract]
    public enum KinectTrackState {
        None = 0, Tracked = 1, Inferred = 2
    }

    [ProtoContract]
    public enum KinectJointType {
        SpineBase = 0, SpineMid = 1, Neck = 2, Head = 3,
        ShoulderLeft = 4, ElbowLeft = 5, WristLeft = 6,
        HandLeft = 7, ShoulderRight = 8, ElbowRight = 9,
        WristRight = 10, HandRight = 11, HipLeft = 12,
        KneeLeft = 13, AnkleLeft = 14, FootLeft = 15,
        HipRight = 16, KneeRight = 17, AnkleRight = 18,
        FootRight = 19, SpineShoulder = 20, HandTipLeft = 21,
        ThumbLeft = 22, HandTipRight = 23, ThumbRight = 24,
    }

    [ProtoContract(SkipConstructor = true, UseProtoMembersOnly = true)]
    public class KinectBody {
        [ProtoMember(1)]
        private float[] _points;
        [ProtoMember(2)]
        public ulong trackingid;
        [ProtoMember(3)]
        public KinectTrackState[] trackStates;
        [ProtoMember(4)]
        private float[] _orientations;
        /*W X Y Z*/
        [ProtoMember(5)]
        public bool[] isActive;
        [ProtoMember(6)]
        public bool isTracked;
        [ProtoMember(7)]
        private float[] _mapped_points;

        public float[,] points {
            get {
                return get(25, 3, _points);
            }
            set {
                set(_points, value);
            }
        }
        public float[,] orientations {
            get {
                return get(25, 4, _orientations);
            }
            set {
                set(_orientations, value);
            }
        }

        public float[,] mapped_points {
            get {
                return get(25, 2, _mapped_points);
            }
            set {
                set(_mapped_points, value);
            }
        }

        private float[,] get(int rows, int cols, float[] source) {
            float[,] retVal = new float[rows, cols];
            Buffer.BlockCopy(source, 0, retVal, 0, rows * cols * sizeof(float));
            return retVal;
        }

        private void set(float[] target, float[,] input) {
            Buffer.BlockCopy(input, 0, target, 0, target.Length * sizeof(float));
        }

        public KinectBody() {
            _points = new float[25 * 3];
            _orientations = new float[25 * 4];
            _mapped_points = new float[25 * 2];
            trackStates = new KinectTrackState[25];
            isActive = new bool[25];
            for (int i = 0; i < 25; i++)
                isActive[i] = false;
            isTracked = false;
        }
    }
    public sealed class KinectClientFrameArrivedEventArgs : EventArgs {
        public FrameType frameType { get; set; }
        public KinectClientInterface source_interface { get; set; }
    }
}
