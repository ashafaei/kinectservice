﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartHomeCore.Networking;
using SmartHomeCore.Kinect;
using System.IO;
using System.Net;
using System.Net.Sockets;
using ProtoBuf;
using System.Threading;
using Lz4Net;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing.Imaging;
using Microsoft.Kinect;
using System.Collections;

namespace SmartHomeCore.Kinect {
    public class KinectProcessing {
        public static BitmapFrame decodeJPG(byte[] data) {
            JpegBitmapDecoder jdecoder = new JpegBitmapDecoder(new MemoryStream(data),
                                                            BitmapCreateOptions.IgnoreImageCache, BitmapCacheOption.None);
            return jdecoder.Frames.ElementAt<BitmapFrame>(0);
        }
        public static byte[] decompressData(byte[] data) {
            return Lz4.DecompressBytes(data);
        }
        public static float[,] deflatePointCloud(byte[] comp_data) {
            byte[] data = decompressData(comp_data);
            if (data.Length % 6 != 0) {
                throw new Exception("Point cloud data must be dividable by 6");
            }
            int length = data.Length / 6;
            float[,] retVal = new float[length, 3];
            unsafe {
                fixed (byte* ptr = data) {
                    short* short_data = (short*)ptr;
                    for (int i = 0; i < length; i++) {
                        retVal[i, 0] = (float)short_data[i * 3];
                        retVal[i, 1] = (float)short_data[i * 3 + 1];
                        retVal[i, 2] = (float)short_data[i * 3 + 2];
                    }
                }
            }
            return retVal;
        }

        private static float InfraredSourceValueMaximum = (float)ushort.MaxValue;
        private static float InfraredSourceScale = 15f;
        private static float InfraredOutputValueMinimum = 1f / 255f;
        private static float InfraredOutputValueMaximum = 1.0f;
        private static int max_persons = 6;

        public static BitmapFrame generateIRImage(byte[] data) {
            WriteableBitmap wbp = new WriteableBitmap(512, 424, 96, 96, PixelFormats.Gray8, null);
            IntPtr dataPtr = System.Runtime.InteropServices.Marshal.UnsafeAddrOfPinnedArrayElement(data, 0);
            IntPtr imgBuffer = wbp.BackBuffer;
            unsafe {
                ushort* irdata = (ushort*)dataPtr;
                byte* imdata = (byte*)imgBuffer;
                for (int i = 0; i < (int)(data.Length / 2); ++i) {
                    //imdata[i] = (byte) Math.Min(InfraredOutputValueMaximum, ((float)irdata[i] / InfraredSourceValueMaximum * InfraredSourceScale));
                    imdata[i] = (byte)(255 * Math.Min(InfraredOutputValueMaximum, (((float)irdata[i] / InfraredSourceValueMaximum * InfraredSourceScale) * (1.0f - InfraredOutputValueMinimum)) + InfraredOutputValueMinimum));
                }
            }

            return BitmapFrame.Create(wbp);
        }

        unsafe public static void segregatePointcloud(ushort* depth_data, byte[] human_index_data, int n_depth_data,
                                                 ushort minReliable, ushort maxReliable, PointF[] mapping,
                                                 out ArrayList[] people, out ArrayList background) {
            people = new ArrayList[max_persons];
            background = new ArrayList();
            for (int i = 0; i < max_persons; i++) {
                people[i] = new ArrayList();
            }

            for (int i = 0; i < n_depth_data; i++) {
                if (depth_data[i] >= minReliable && depth_data[i] <= maxReliable) {
                    if (human_index_data[i] < max_persons) {
                        people[human_index_data[i]].Add(new float[]{mapping[i].X*depth_data[i],
                                                                    mapping[i].Y*depth_data[i],
                                                                    depth_data[i]});
                    }
                    else {
                        background.Add(new float[]{mapping[i].X*depth_data[i],
                                                                    mapping[i].Y*depth_data[i],
                                                                    depth_data[i]});

                    }
                }
            }
        }
    }

}
