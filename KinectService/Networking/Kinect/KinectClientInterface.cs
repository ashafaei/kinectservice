﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using SmartHomeCore.Kinect;
using Emgu.CV;
using Emgu.CV.Structure;
using SmartHomeCore.PointCloud;
using System.Windows.Media.Imaging;

namespace SmartHomeCore.Networking.Kinect {

    public class Point3D {
        public float X, Y, Z;
        public override string ToString() {
            return X + "," + Y + "," + Z;
        }
    }

    public struct Point2D {
        public float X, Y;
    }

    public interface KinectClientInterface {
        event EventHandler<KinectClientFrameArrivedEventArgs> FrameArrived;
        Point2D[] getDepthToColorMap();

        ushort getMinReliable();
        ushort getMaxReliable();

        void Start();
        void ResetBaseTime();
        void Stop();

        void convertDepthToPointcloud(byte[] depth_data_binary, ushort minReliable, ushort maxReliable,   
                                      BufferedPointCloud buffered_cloud);
        byte[,] getColorForDepthIds(int[] depth_idsm, Image<Bgr, byte> image);
        bool isEnabled(SmartHomeCore.Kinect.FrameType frameType);

        float[,] getDeflatedPointCloud(out double timestamp);
        BitmapFrame getDecodedImage(out double timestamp);
        byte[] getBodyIndexMapping(out double timestamp);

        byte[] getDepthData(out double timestamp);

        byte[] getIRData(out double timestmap);

        KinectBody[] getKinectBodies(out double timestamp);

    }
}
