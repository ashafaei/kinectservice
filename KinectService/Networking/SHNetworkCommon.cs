﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using System.IO;
using System.Threading;

namespace SmartHomeCore.Networking
{
    [ProtoContract]
    public enum MessageType
    {
        Hello, Bye
    }

    [ProtoContract]
    public class SHMessage
    {
        [ProtoMember(1)]
        public MessageType MessgeType { get; set; }

        public override string ToString()
        {
            return "Message Type: " + MessgeType;
        }
    }

    public class SHNetworkCommon
    {
        private Dictionary<int, Type> dynLookup = new Dictionary<int, Type> { { 1, typeof(SHMessage) } };
        private Dictionary<Type, int> invDynLookup = new Dictionary<Type, int> { { typeof(SHMessage), 1 } };
        private Serializer.TypeResolver dynResolver = null;

        public SHNetworkCommon()
        {
            this.dynResolver = i => { Type t; return dynLookup.TryGetValue(i, out t) ? t : null; };
        }

        public void RegisterType(Type t)
        {
            int id = dynLookup.Count + 1;
            dynLookup.Add(id, t);
            invDynLookup.Add(t, id);
        }

        public bool SendMessage(Stream stream, object message)
        {
            try
            {
                int type = (invDynLookup.TryGetValue(message.GetType(), out type) ? type : 0);
                Serializer.NonGeneric.SerializeWithLengthPrefix(stream, message, PrefixStyle.Base128, type);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }


        public T ReadMessage<T>(Stream stream)
        {
            Object response = null;
            try
            {
                while (Serializer.NonGeneric.TryDeserializeWithLengthPrefix(stream, PrefixStyle.Base128, dynResolver, out response))
                {
                    if (response == null)
                    {
                        Thread.Sleep(20);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {

            }
            return (T)response;
        }
    }
}
