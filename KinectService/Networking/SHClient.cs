﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using ProtoBuf;

namespace SmartHomeCore.Networking
{
    public class SHClient
    {
        private TcpClient client;
        private NetworkStream stream;
        private string address;
        private int port;
        private SHNetworkCommon networkCommon { get; set; }

        public SHClient(string address, int port)
        {
            this.address = address;
            this.port = port;
            this.networkCommon = new SHNetworkCommon();
        }

        public void RegisterType(Type t)
        {
            networkCommon.RegisterType(t);
        }

        public void DispatchMessage(Object message)
        {
            networkCommon.SendMessage(this.stream, message);
        }

        public T RetrieveMessage<T>()
        {
            return networkCommon.ReadMessage<T>(this.stream);
        }

        public bool Start()
        {
            Console.WriteLine("Connecting to " + this.address + ":" + this.port);
            this.client = new TcpClient(this.address, this.port);
            Console.WriteLine("Connected to " + this.address + ":" + this.port);
            this.stream = this.client.GetStream();
            try
            {
                Console.WriteLine("Sayin hello to server");
                networkCommon.SendMessage(this.stream, new SHMessage { MessgeType = MessageType.Hello });
                Console.WriteLine("Waiting for response");
                SHMessage response = networkCommon.ReadMessage<SHMessage>(this.stream);
                Console.WriteLine("Response from server received!");
                Console.WriteLine(response);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occured! " + e.Message);
                return false;
            }
            return true;
        }

        public void Stop()
        {
            if (this.client != null)
            {
                Console.WriteLine("Closing connection to " + this.address + ":" + this.port);
                networkCommon.SendMessage(this.stream, new SHMessage { MessgeType = MessageType.Bye });
                if (this.client.Connected)
                {
                    this.client.Close();
                }
            }
        }

    }
}
