﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using ProtoBuf;

namespace SmartHomeCore.Networking
{

    public class Messenger
    {
        private SHNetworkCommon networkCommon;
        private NetworkStream stream;
        private TcpClient client;

        public Messenger(SHNetworkCommon networkCommon, NetworkStream stream, TcpClient client)
        {
            this.networkCommon = networkCommon;
            this.stream = stream;
            this.client = client;
        }

        public void DispatchMessage(Object message)
        {
            lock (this)
            {
                networkCommon.SendMessage(this.stream, message);
            }
        }

        public T RetrieveMessage<T>()
        {
            return networkCommon.ReadMessage<T>(this.stream);
        }

        public void Close()
        {
            client.Close();
        }
    }
    public class SHServer
    {
        private TcpListener server;
        private int listen_port;
        private volatile bool isActive = false;

        public delegate void ProcessRequest(Messenger messenger);

        private ProcessRequest requestHandler = null;
        private SHNetworkCommon networkCommon { get; set; }

        public SHServer(int port)
        {
            this.listen_port = port;
            this.networkCommon = new SHNetworkCommon();
        }
        public void RegisterType(Type t)
        {
            networkCommon.RegisterType(t);
        }

        public void Start()
        {
            if (!isActive)
            {
                isActive = true;
                new Thread(Serve).Start();
            }
        }

        public void Stop()
        {
            if (isActive)
            {
                isActive = false;
            }
        }

        public void SetProcessHandler(ProcessRequest requestHandler)
        {
            this.requestHandler = requestHandler;
        }

        private void Serve()
        {
            this.server = new TcpListener(IPAddress.Any, this.listen_port);
            this.server.Start();
            Console.WriteLine("Starting server on port " + this.listen_port);
            while (isActive)
            {
                if (!this.server.Pending())
                {
                    Thread.Sleep(500);
                    continue;
                }
                TcpClient c = this.server.AcceptTcpClient();
                Console.WriteLine("Connection from " + c.Client.RemoteEndPoint + " arrived! ");
                try
                {
                    NetworkStream ns = c.GetStream();
                    Console.WriteLine("Waiting for client");
                    Console.WriteLine("Deserializing");
                    SHMessage msg = networkCommon.ReadMessage<SHMessage>(ns);
                    Console.WriteLine("Message received (" + msg + ")\nResponding");
                    networkCommon.SendMessage(ns, msg);
                    Console.WriteLine("Response sent!");
                    if (requestHandler != null)
                    {
                        Console.WriteLine("Diverting client");
                        new Thread(() => requestHandler(new Messenger(this.networkCommon, ns, c))).Start();
                    }
                    else
                    {
                        c.Close();
                        ((IDisposable)c).Dispose();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    ((IDisposable)c).Dispose();
                }
            }
            Console.WriteLine("Closing server!");
            this.server.Stop();
        }
    }
}
