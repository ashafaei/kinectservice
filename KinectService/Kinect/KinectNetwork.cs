﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartHomeCore.Networking;
using SmartHomeCore.Kinect;
using System.IO;
using System.Net;
using System.Net.Sockets;
using ProtoBuf;
using Microsoft.Kinect;
using System.Threading;
using Lz4Net;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing.Imaging;
using System.Collections;
using SmartHomeCore.Networking.Kinect;
using Emgu.CV;
using Emgu.CV.Structure;
using SmartHomeCore.PointCloud;

namespace KinectService.Kinect {
    [ProtoContract]
    enum KinectMessageType {
        Hello_Kinect, Bye_Kinect, Reset_BaseTime,

        Conversion_Tables,

        Subscribe_RGB, Unsubscribe_RGB, RGB_data,

        Subscribe_IR, Unsubscribe_IR, IR_data,

        Subscribe_BodyIndex, Unsubscibe_BodyIndex, BodyIndex_data,

        Subscribe_Depth, Unsubscribe_Depth, Depth_data,

        Subscribe_Point_Cloud, Unsubscribe_Point_Cloud, Point_Cloud_Data,

        Subscribe_body, Unsubscribe_body, Body_data
    }

    [ProtoContract(SkipConstructor = true, UseProtoMembersOnly = true)]
    class KinectMessage {
        [ProtoMember(1)]
        public KinectMessageType messageType { get; set; }
        [ProtoMember(2)]
        public KinectMessageParams param { get; set; }
        [ProtoMember(3)]
        public byte[] payload { get; set; }
        [ProtoMember(4)]
        public KinectBody[] bodies { get; set; }
        [ProtoMember(5)]
        public float[] floorplane { get; set; }

        public static KinectMessage create(KinectMessageType type, KinectMessageParams param = null, byte[] payload = null) {
            return new KinectMessage {
                messageType = type,
                param = param,
                payload = payload,
            };
        }

        public static KinectMessage ResetBaseTime() {
            return KinectMessage.create(KinectMessageType.Reset_BaseTime);
        }
    }

    /* KinectMessageParams:
     *  For Subscribe_RGB
     *      int_param_1 = update_interval;
     *      int_param_2 = JpegQuality x/100;
     * 
     */
    [ProtoContract(SkipConstructor = true, UseProtoMembersOnly = true)]
    class KinectMessageParams {
        [ProtoMember(1)]
        private int int_param_1;
        [ProtoMember(2)]
        private int int_param_2;
        [ProtoMember(3)]
        private float float_param_1;
        [ProtoMember(4)]
        private double double_param_1;
        public int rgb_update_interval { get { return int_param_1; } set { int_param_1 = value; } }
        public int jpeg_quality { get { return int_param_2; } set { int_param_2 = value; } }
        public float size_ratio { get { return float_param_1; } set { float_param_1 = value; } }
        public double time_stamp { get { return double_param_1; } set { double_param_1 = value; } }
        public int body_update_interval { get { return int_param_1; } set { int_param_1 = value; } }
        public int bodyIndex_update_interval { get { return int_param_1; } set { int_param_1 = value; } }
        public int depth_update_interval { get { return int_param_1; } set { int_param_1 = value; } }
        public int ir_update_interval { get { return int_param_1; } set { int_param_1 = value; } }
        public int depth_max_reliable { get { return int_param_2; } set { int_param_2 = (int)value; } }
        public int depth_min_reliable { get { return (int)float_param_1; } set { float_param_1 = (float)value; } }
        public int point_cloud_update_interval { get { return int_param_1; } set { int_param_1 = value; } }
        public int rgb_w { get { return int_param_1; } set { int_param_1 = value; } }
        public int rgb_h { get { return int_param_2; } set { int_param_2 = value; } }
        public int depth_w { get { return (int)float_param_1; } set { float_param_1 = (float)value; } }
        public int depth_h { get { return (int)double_param_1; } set { double_param_1 = (double)value; } }

        public static KinectMessageParams SubscribeRGBParams(int rgb_update_interval, int jpeg_quality = 70, float size_ratio = 1) {
            KinectMessageParams retVal = new KinectMessageParams();
            retVal.rgb_update_interval = rgb_update_interval;
            retVal.jpeg_quality = jpeg_quality;
            retVal.size_ratio = size_ratio;
            return retVal;
        }

        public static KinectMessageParams SubscribeBodyIndexParams(int update_interval = 100) {
            return new KinectMessageParams { bodyIndex_update_interval = update_interval };
        }

        public static KinectMessageParams SubscribeDepthParams(int update_interval = 100) {
            return new KinectMessageParams { depth_update_interval = update_interval };
        }

        public static KinectMessageParams SubscribePointCloudParams(int update_interval = 100) {
            return new KinectMessageParams { point_cloud_update_interval = update_interval };
        }

        public static KinectMessageParams SubscribeIRParams(int update_interval = 100) {
            return new KinectMessageParams { ir_update_interval = update_interval };
        }

        public static KinectMessageParams FrameParams(double timestamp) {
            KinectMessageParams retVal = new KinectMessageParams();
            retVal.time_stamp = timestamp;
            return retVal;
        }

    }

    class KinectClientOnServer {
        public volatile bool RGB = false, bodyIndex = false, depth = false, point_cloud = false, ir_data = false, body_data = false;
        public KinectMessageParams RGB_params, bodyIndexParams, depthParams, pointCloudParams, irParams, bodyParams;
        private long last_RGB, last_BodyIndex, last_depth, last_point_cloud, last_ir, last_body;
        private byte[] RGB_buffer = null, bodyIndex_buffer = null, depth_buffer = null, point_cloud_buffer = null, ir_buffer = null;
        private double frame_base = 0;
        private double last_frame = 0;
        private WriteableBitmap colorBitmap;
        private PointF[] conversion_table_depth_to_pc;
        private KinectNode kn;

        public Messenger Messenger = null;

        public KinectClientOnServer(Messenger messenger) {
            this.Messenger = messenger;
            last_RGB = last_BodyIndex = last_point_cloud = last_ir = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        private static BitmapFrame CreateResizedImage(ImageSource source, int width, int height, int margin) {
            var rect = new Rect(margin, margin, width - margin * 2, height - margin * 2);

            var group = new DrawingGroup();
            RenderOptions.SetBitmapScalingMode(group, BitmapScalingMode.HighQuality);
            group.Children.Add(new ImageDrawing(source, rect));

            var drawingVisual = new DrawingVisual();
            using (var drawingContext = drawingVisual.RenderOpen())
                drawingContext.DrawDrawing(group);

            var resizedImage = new RenderTargetBitmap(
                width, height,         // Resized dimensions
                96, 96,                // Default DPI values
                PixelFormats.Default); // Default pixel format
            resizedImage.Render(drawingVisual);

            return BitmapFrame.Create(resizedImage);
        }

        public void ResetBaseTime() {
            this.frame_base = this.last_frame;
        }

        private bool checkTime(ref long last_stamp, long interval) {
            long current_time = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            if (current_time - last_stamp < interval) {
                //Console.WriteLine("Was expecting " + interval + " got " + (current_time - last_stamp));
                return false;
            }
            last_stamp = current_time;
            return true;
        }

        public void RGBFrameArrived(ColorFrameReference colorFrameReference) {
            if (!checkTime(ref this.last_RGB, this.RGB_params.rgb_update_interval)) {
                return;
            }

            // Console.WriteLine("Acquiring frame!");
            bool updated = false; long length = 0;
            using (ColorFrame colorFrame = colorFrameReference.AcquireFrame()) {
                if (colorFrame != null) {
                    FrameDescription colorFrameDescription = colorFrame.FrameDescription;
                    if (this.RGB_buffer == null) {
                        uint buffer_size = (uint)colorFrameDescription.Width * (uint)colorFrameDescription.Height * 4;
                        Console.WriteLine("Allocating " + buffer_size + " bytes for the RGB buffer");
                        this.RGB_buffer = new byte[buffer_size];

                        this.colorBitmap = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);
                        this.frame_base = colorFrameReference.RelativeTime.TotalMilliseconds;
                    }

                    using (KinectBuffer colorBuffer = colorFrame.LockRawImageBuffer()) {
                        // Console.WriteLine("Compressing image");
                        long st_time = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

                        colorFrame.CopyConvertedFrameDataToIntPtr(
                            this.colorBitmap.BackBuffer,
                            (uint)(colorFrameDescription.Width * colorFrameDescription.Height * 4),
                            ColorImageFormat.Bgra);

                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = this.RGB_params.jpeg_quality;
                        BitmapFrame bf = BitmapFrame.Create(this.colorBitmap);
                        if (this.RGB_params.size_ratio < 1) {
                            bf = KinectClientOnServer.CreateResizedImage(bf, (int)(1920 * this.RGB_params.size_ratio), (int)(1080 * this.RGB_params.size_ratio), 0);
                        }
                        encoder.Frames.Add(bf);
                        MemoryStream memStream = new MemoryStream(this.RGB_buffer);
                        encoder.Save(memStream);
                        length = memStream.Position;
                        // Console.WriteLine("Compression complete " + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - st_time));

                        updated = true;
                    }
                }
            }
            if (updated) {
                //Console.WriteLine("Transmitting the image " + length);
                byte[] payload = new byte[length];
                Buffer.BlockCopy(this.RGB_buffer, 0, payload, 0, (int)length);
                TimeSpan time = colorFrameReference.RelativeTime;
                Messenger.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.RGB_data,
                    param = KinectMessageParams.FrameParams(time.TotalMilliseconds - this.frame_base),
                    payload = payload
                });
            }
        }

        public void BodyIndexFrameArrived(BodyIndexFrameReference bodyFrameReference) {
            if (!checkTime(ref this.last_BodyIndex, this.bodyIndexParams.bodyIndex_update_interval)) {
                return;
            }
            bool updated = false;
            byte[] out_buffer = null;
            using (BodyIndexFrame bodyFrame = bodyFrameReference.AcquireFrame()) {
                if (bodyFrame != null) {
                    FrameDescription frameDescription = bodyFrame.FrameDescription;
                    if (this.bodyIndex_buffer == null) {
                        uint buffer_size = (uint)frameDescription.Width * (uint)frameDescription.Height;
                        Console.WriteLine("Allocating " + buffer_size + " bytes for the bodyIndex buffer");
                        this.bodyIndex_buffer = new byte[buffer_size];
                    }

                    using (KinectBuffer bodyIndexBuffer = bodyFrame.LockImageBuffer()) {
                        //Console.WriteLine("Compressing bodyIndex");
                        long st_time = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                        System.Runtime.InteropServices.Marshal.Copy(bodyIndexBuffer.UnderlyingBuffer, this.bodyIndex_buffer, 0, (int)bodyIndexBuffer.Size);
                        out_buffer = Lz4.CompressBytes(this.bodyIndex_buffer, Lz4Mode.HighCompression);
                        //Console.WriteLine("Compression complete " + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - st_time));
                        updated = true;
                    }
                }
            }
            if (updated) {
                int length = out_buffer.Length;
                //Console.WriteLine("Transmitting the bodyIndex " + this.bodyIndex_buffer.Length + " -> " + length);
                TimeSpan time = bodyFrameReference.RelativeTime;
                Messenger.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.BodyIndex_data,
                    param = KinectMessageParams.FrameParams(time.TotalMilliseconds - this.frame_base),
                    payload = out_buffer
                });
            }
        }

        public void DepthFrameArrived(DepthFrameReference depthFrameReference) {
            if (!checkTime(ref this.last_depth, this.depthParams.depth_update_interval)) {
                return;
            }
            bool updated = false;
            byte[] out_buffer = null;
            int max_depth = 0, min_depth = 0;
            using (DepthFrame depthFrame = depthFrameReference.AcquireFrame()) {
                if (depthFrame != null) {
                    FrameDescription frameDescription = depthFrame.FrameDescription;
                    if (this.depth_buffer == null) {
                        uint buffer_size = (uint)frameDescription.Width * (uint)frameDescription.Height * sizeof(ushort);
                        Console.WriteLine("Allocating " + buffer_size + " bytes for the depth buffer");
                        this.depth_buffer = new byte[buffer_size];
                    }

                    using (KinectBuffer depthBuffer = depthFrame.LockImageBuffer()) {
                        //Console.WriteLine("Compressing depth");
                        long st_time = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                        System.Runtime.InteropServices.Marshal.Copy(depthBuffer.UnderlyingBuffer, this.depth_buffer, 0, (int)depthBuffer.Size);
                        out_buffer = Lz4.CompressBytes(this.depth_buffer, Lz4Mode.HighCompression);
                        //Console.WriteLine("Compression complete " + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - st_time));
                        max_depth = (int)depthFrame.DepthMaxReliableDistance;
                        min_depth = (int)depthFrame.DepthMinReliableDistance;
                        updated = true;
                    }
                }
            }
            if (updated) {
                int length = out_buffer.Length;
                // Console.WriteLine("Transmitting the depth " + this.depth_buffer.Length + " -> " + length);
                TimeSpan time = depthFrameReference.RelativeTime;
                KinectMessageParams param = KinectMessageParams.FrameParams(time.TotalMilliseconds - this.frame_base);
                param.depth_max_reliable = max_depth;
                param.depth_min_reliable = min_depth;
                Messenger.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.Depth_data,
                    param = param,
                    payload = out_buffer
                });
            }
        }

        public void IRFrameArrived(InfraredFrameReference irFrameReference) {
            if (!checkTime(ref this.last_ir, this.irParams.ir_update_interval)) {
                return;
            }
            bool updated = false;
            byte[] out_buffer = null;
            using (InfraredFrame irFrame = irFrameReference.AcquireFrame()) {
                if (irFrame != null) {
                    FrameDescription frameDescription = irFrame.FrameDescription;
                    if (this.depth_buffer == null) {
                        uint buffer_size = (uint)frameDescription.Width * (uint)frameDescription.Height * sizeof(ushort);
                        Console.WriteLine("Allocating " + buffer_size + " bytes for the ir buffer");
                        this.depth_buffer = new byte[buffer_size];
                    }

                    using (KinectBuffer irBuffer = irFrame.LockImageBuffer()) {
                        //Console.WriteLine("Compressing depth");
                        long st_time = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                        System.Runtime.InteropServices.Marshal.Copy(irBuffer.UnderlyingBuffer, this.depth_buffer, 0, (int)irBuffer.Size);
                        out_buffer = Lz4.CompressBytes(this.depth_buffer, Lz4Mode.HighCompression);
                        //Console.WriteLine("Compression complete " + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - st_time));
                        updated = true;
                    }
                }
            }
            if (updated) {
                int length = out_buffer.Length;
                // Console.WriteLine("Transmitting the depth " + this.depth_buffer.Length + " -> " + length);
                TimeSpan time = irFrameReference.RelativeTime;
                KinectMessageParams param = KinectMessageParams.FrameParams(time.TotalMilliseconds - this.frame_base);
                Messenger.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.IR_data,
                    param = param,
                    payload = out_buffer
                });
            }
        }

        public void PointCloudDepthArrived(DepthFrameReference depthFrameReference) {
            if (!checkTime(ref this.last_point_cloud, this.pointCloudParams.point_cloud_update_interval)) {
                return;
            }
            bool updated = false;
            byte[] out_buffer = null;
            using (DepthFrame depthFrame = depthFrameReference.AcquireFrame()) {
                if (depthFrame != null) {
                    FrameDescription frameDescription = depthFrame.FrameDescription;
                    if (this.point_cloud_buffer == null) {
                        uint buffer_size = (uint)frameDescription.Width * (uint)frameDescription.Height * sizeof(short) * 3;
                        // Console.WriteLine("Allocating " + buffer_size + " bytes for the point cloud buffer");
                        this.point_cloud_buffer = new byte[buffer_size];
                    }

                    using (KinectBuffer depthBuffer = depthFrame.LockImageBuffer()) {
                        int last_index = 0;
                        unsafe {
                            fixed (byte* buffer = this.point_cloud_buffer) {
                                ushort* depth_data = (ushort*)depthBuffer.UnderlyingBuffer;
                                short* short_buffer = (short*)buffer;
                                for (int i = 0; i < depthBuffer.Size / 2; i++) {
                                    if (depth_data[i] > depthFrame.DepthMinReliableDistance && depth_data[i] < ushort.MaxValue /*depthFrame.DepthMaxReliableDistance*/) {
                                        short_buffer[last_index++] = (short)(this.conversion_table_depth_to_pc[i].X * depth_data[i]);
                                        short_buffer[last_index++] = (short)(this.conversion_table_depth_to_pc[i].Y * depth_data[i]);
                                        short_buffer[last_index++] = (short)(depth_data[i]);
                                    }
                                }

                            }
                        }
                        // Console.WriteLine("Compressing depth");
                        long st_time = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                        out_buffer = Lz4.CompressBytes(this.point_cloud_buffer, 0, last_index * 2, Lz4Mode.HighCompression);
                        //Console.WriteLine("Compression complete " + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - st_time));
                        updated = true;
                    }
                }
            }
            if (updated) {
                int length = out_buffer.Length;
                //Console.WriteLine("Transmitting the PointCloud " + this.point_cloud_buffer.Length + " -> " + length);
                TimeSpan time = depthFrameReference.RelativeTime;
                KinectMessageParams param = KinectMessageParams.FrameParams(time.TotalMilliseconds - this.frame_base);
                Messenger.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.Point_Cloud_Data,
                    param = param,
                    payload = out_buffer
                });
            }
        }

        private Body[] bodies = new Body[6];

        public void BodyFrameArrived(BodyFrameReference bodyFrameReference) {
            if (!checkTime(ref this.last_body, this.bodyParams.body_update_interval)) {
                return;
            }
            Vector4 fp; KinectBody[] kinectBodies; TimeSpan time;
            using (BodyFrame bf = bodyFrameReference.AcquireFrame()) {
                if (bf == null) {
                    return;
                }
                bf.GetAndRefreshBodyData(bodies);
                kinectBodies = new KinectBody[6];
                int index = 0;
                foreach (Body b in bodies) {
                    kinectBodies[index] = new KinectBody();
                    if (!b.IsTracked) {
                        continue;
                    }
                    IReadOnlyDictionary<JointType, Joint> joints = b.Joints;
                    kinectBodies[index].trackingid = b.TrackingId;
                    kinectBodies[index].isTracked = true;
                    float[,] joint_points = kinectBodies[index].points;
                    float[,] mapped_points = kinectBodies[index].mapped_points;
                    foreach (JointType jt in joints.Keys) {
                        Joint point = joints[jt];
                        CameraSpacePoint csp = point.Position;
                        if (csp.Z < 0) {
                            csp.Z = 0.1f;
                        }
                        DepthSpacePoint dsp = this.kn.mapper.MapCameraPointToDepthSpace(csp);
                        joint_points[(int)jt, 0] = csp.X*1000; // It's in meters, converting back to milimeters!
                        joint_points[(int)jt, 1] = csp.Y*1000;
                        joint_points[(int)jt, 2] = csp.Z*1000;
                        mapped_points[(int)jt, 0] = dsp.X;
                        mapped_points[(int)jt, 1] = dsp.Y;
                        kinectBodies[index].trackStates[(int)jt] = (KinectTrackState)point.TrackingState;
                        kinectBodies[index].isActive[(int)jt] = true;
                    }
                    kinectBodies[index].points = joint_points;
                    kinectBodies[index].mapped_points = mapped_points;
                    IReadOnlyDictionary<JointType, JointOrientation> orientations = b.JointOrientations;
                    float[,] jointOrientations = kinectBodies[index].orientations;
                    foreach (JointType ht in orientations.Keys) {
                        JointOrientation jo = orientations[ht];
                        jointOrientations[(int)ht, 0] = jo.Orientation.W;
                        jointOrientations[(int)ht, 1] = jo.Orientation.X;
                        jointOrientations[(int)ht, 2] = jo.Orientation.Y;
                        jointOrientations[(int)ht, 3] = jo.Orientation.Z;
                    }
                    kinectBodies[index].orientations = jointOrientations;
                    index++;
                }
                fp = bf.FloorClipPlane;
                time = bodyFrameReference.RelativeTime;
            }
            KinectMessage message = new KinectMessage {
                messageType = KinectMessageType.Body_data,
                floorplane = new float[4] { fp.W, fp.X, fp.Y, fp.Z },
                bodies = kinectBodies,
                param = KinectMessageParams.FrameParams(time.TotalMilliseconds - this.frame_base)
            };
            Messenger.DispatchMessage(message);
        }

        public void TransmitConversionTable(KinectNode kinect) {
            PointF[] depth_to_space = kinect.depthTable;
            DepthSpacePoint[] color_to_depth = kinect.colorMappedToDepthPoints;
            ColorSpacePoint[] depth_to_color = kinect.depthMappedToColorSpacePoints;
            byte[] buffer = new byte[depth_to_space.Length * sizeof(float) * 2 +
                                    color_to_depth.Length * sizeof(float) * 2 +
                                    depth_to_color.Length * sizeof(float) * 2];
            MemoryStream memStream = new MemoryStream(buffer);
            BinaryWriter writer = new BinaryWriter(memStream);
            for (int i = 0; i < depth_to_space.Length; i++) {
                writer.Write(depth_to_space[i].X);
                writer.Write(depth_to_space[i].Y);
            }
            for (int i = 0; i < color_to_depth.Length; i++) {
                writer.Write(color_to_depth[i].X);
                writer.Write(color_to_depth[i].Y);
            }
            for (int i = 0; i < depth_to_color.Length; i++) {
                writer.Write(depth_to_color[i].X);
                writer.Write(depth_to_color[i].Y);
            }
            writer.Close();
            memStream.Close();
            byte[] out_buffer = Lz4.CompressBytes(buffer, 0, buffer.Length, Lz4Mode.HighCompression);
            Messenger.DispatchMessage(new KinectMessage {
                messageType = KinectMessageType.Conversion_Tables,
                param = null,
                payload = out_buffer
            });
        }
        public void Register(KinectNode kinect) {
            kinect.kinectMultiSourceFrameReader.MultiSourceFrameArrived += this.kinectMultiSourceFrameReader_MultiSourceFrameArrived;
            this.kn = kinect;
            this.conversion_table_depth_to_pc = kinect.depthTable;
            TransmitConversionTable(kinect);
        }

        void kinectMultiSourceFrameReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e) {
            MultiSourceFrame msf = e.FrameReference.AcquireFrame();
            if (msf.ColorFrameReference != null) {
                ColorFrameReference cf = msf.ColorFrameReference;
                this.last_frame = cf.RelativeTime.TotalMilliseconds;
            }
            if (RGB && msf.ColorFrameReference != null) {
                RGBFrameArrived(msf.ColorFrameReference);
            }
            if (bodyIndex && msf.BodyIndexFrameReference != null) {
                BodyIndexFrameArrived(msf.BodyIndexFrameReference);
            }
            if (depth && msf.DepthFrameReference != null) {
                DepthFrameArrived(msf.DepthFrameReference);
            }
            if (point_cloud && msf.DepthFrameReference != null) {
                PointCloudDepthArrived(msf.DepthFrameReference);
            }

            if (ir_data && msf.InfraredFrameReference != null) {
                IRFrameArrived(msf.InfraredFrameReference);
            }
            if (body_data && msf.BodyFrameReference != null) {
                BodyFrameArrived(msf.BodyFrameReference);
            }
        }

        public void Unregister(KinectNode kinect) {
            kinect.kinectMultiSourceFrameReader.MultiSourceFrameArrived -= this.kinectMultiSourceFrameReader_MultiSourceFrameArrived;
        }
    }


    public class KinectServer {
        private KinectNode kinect = null;
        private SHServer server = null;
        public KinectServer(KinectNode kinect, int port) {
            this.kinect = kinect;
            this.server = new SHServer(port);
            this.server.SetProcessHandler(HandleClient);
            this.server.RegisterType(typeof(KinectMessage));
            this.server.RegisterType(typeof(KinectMessageParams));
            this.server.RegisterType(typeof(KinectBody));
        }

        public void Start() {
            this.server.Start();
        }

        public void Stop() {
            this.server.Stop();
        }

        private void HandleClient(Messenger client) {
            Console.WriteLine("Waiting for the Kinect Client to say hi! ");
            KinectMessage message = client.RetrieveMessage<KinectMessage>();
            Console.WriteLine("Kinect client said " + message.messageType);
            message.param = new KinectMessageParams {
                rgb_w = this.kinect.colorWidth,
                rgb_h = this.kinect.colorHeight,
                depth_w = this.kinect.depthWidth,
                depth_h = this.kinect.depthHeight
            };
            client.DispatchMessage(message);
            bool isActive = true;
            KinectClientOnServer kinectClient = new KinectClientOnServer(client);
            kinectClient.Register(this.kinect);
            while (isActive) {
                message = client.RetrieveMessage<KinectMessage>();
                if (message == null) {
                    isActive = false;
                    client.Close();
                    Console.WriteLine("Client closed!");
                    break;
                }
                Console.WriteLine("Client said " + message.messageType);
                switch (message.messageType) {
                    case KinectMessageType.Subscribe_RGB:
                        kinectClient.RGB = true;
                        kinectClient.RGB_params = message.param;
                        break;
                    case KinectMessageType.Unsubscribe_RGB:
                        kinectClient.RGB = false;
                        break;
                    case KinectMessageType.Subscribe_BodyIndex:
                        kinectClient.bodyIndex = true;
                        kinectClient.bodyIndexParams = message.param;
                        break;
                    case KinectMessageType.Unsubscibe_BodyIndex:
                        kinectClient.bodyIndex = false;
                        break;
                    case KinectMessageType.Subscribe_Depth:
                        kinectClient.depth = true;
                        kinectClient.depthParams = message.param;
                        break;
                    case KinectMessageType.Unsubscribe_Depth:
                        kinectClient.depth = false;
                        break;
                    case KinectMessageType.Subscribe_Point_Cloud:
                        kinectClient.point_cloud = true;
                        kinectClient.pointCloudParams = message.param;
                        break;
                    case KinectMessageType.Unsubscribe_Point_Cloud:
                        kinectClient.point_cloud = false;
                        break;
                    case KinectMessageType.Subscribe_IR:
                        kinectClient.ir_data = true;
                        kinectClient.irParams = message.param;
                        break;
                    case KinectMessageType.Unsubscribe_IR:
                        kinectClient.ir_data = false;
                        break;
                    case KinectMessageType.Subscribe_body:
                        kinectClient.body_data = true;
                        kinectClient.bodyParams = message.param;
                        break;
                    case KinectMessageType.Unsubscribe_body:
                        kinectClient.body_data = false;
                        break;
                    case KinectMessageType.Bye_Kinect:
                        isActive = false;
                        break;
                    case KinectMessageType.Reset_BaseTime:
                        kinectClient.ResetBaseTime();
                        break;
                    default:
                        break;

                }
            }
            kinectClient.Unregister(this.kinect);
            client.Close();
        }

    }
    public class KinectClient : SmartHomeCore.Networking.Kinect.KinectClientInterface {


        private SHClient client = null;
        private bool isActive = false;
        private int min_depth = 50, max_depth = 8000;
        private PointF[] conversion_table_depth_to_pc;
        private DepthSpacePoint[] colorMappedToDepthPoints = null;
        public Point2D[] depthToColor;

        private Dictionary<KinectMessageType, KinectMessage> latest_data = new Dictionary<KinectMessageType, KinectMessage>();
        private Dictionary<KinectMessageType, Tuple<object, double>> cached_data = new Dictionary<KinectMessageType, Tuple<object, double>>();

        private Dictionary<SmartHomeCore.Kinect.FrameType, bool> activations = new Dictionary<FrameType, bool>();
        private KinectBody[] bodies = null;
        private float[] plane = null;

        public Int32Rect depthDataFrameSize = new Int32Rect(0, 0, 512, 424);
        public Int32Rect rgbDataFrameSize = new Int32Rect(0, 0, 1920, 1080);
        public KinectClient(string address, int port) {
            this.client = new SHClient(address, port);
            this.client.RegisterType(typeof(KinectMessage));
            this.client.RegisterType(typeof(KinectMessageParams));
            this.client.RegisterType(typeof(KinectBody));
            for (int i = 0; i < (int)SmartHomeCore.Kinect.FrameType.EOE; i++) {
                activations.Add((SmartHomeCore.Kinect.FrameType)i, false);
            }
            foreach (KinectMessageType kmt in Enum.GetValues(typeof(KinectMessageType))) {
                latest_data[kmt] = null;
                cached_data[kmt] = new Tuple<object, double>(null, -1);
            }
        }

        public void Start() {
            if (!this.client.Start()) {
                throw new Exception("Couldn't connect to server");
            }
            else {
                Console.WriteLine("Kinect client saying HI");
                this.client.DispatchMessage(new KinectMessage { messageType = KinectMessageType.Hello_Kinect });
                KinectMessage response = this.client.RetrieveMessage<KinectMessage>();
                Console.WriteLine("Response received from Kinect Server " + response.messageType);
                this.rgbDataFrameSize = new Int32Rect(0, 0, response.param.rgb_w, response.param.rgb_h);
                this.depthDataFrameSize = new Int32Rect(0, 0, response.param.depth_w, response.param.depth_h);
                this.isActive = true;
                new Thread(mainLoop).Start();
            }

        }

        public KinectClient EnableRGBStream(bool enable, int interval = 100, int JPG_quality = 50, float frame_ratio = 1) {
            activations[SmartHomeCore.Kinect.FrameType.RGB_Data] = enable;
            if (enable) {
                this.client.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.Subscribe_RGB,
                    param = KinectMessageParams.SubscribeRGBParams(interval, JPG_quality, frame_ratio)
                });
            }
            else {
                this.client.DispatchMessage(new KinectMessage { messageType = KinectMessageType.Unsubscribe_RGB });
            }
            return this;
        }

        public KinectClient EnableBodyStream(bool enable, int interval = 100) {
            activations[SmartHomeCore.Kinect.FrameType.body_data] = enable;
            if (enable) {
                this.client.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.Subscribe_body,
                    param = KinectMessageParams.SubscribeIRParams(interval)
                });
            }
            else {
                this.client.DispatchMessage(new KinectMessage { messageType = KinectMessageType.Unsubscribe_body });
            }
            return this;
        }

        public KinectClient EnableBodyIndexStream(bool enable, int interval = 100) {
            activations[SmartHomeCore.Kinect.FrameType.BodyIndex_Data] = enable;
            if (enable) {
                this.client.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.Subscribe_BodyIndex,
                    param = KinectMessageParams.SubscribeBodyIndexParams(interval)
                });
            }
            else {
                this.client.DispatchMessage(new KinectMessage { messageType = KinectMessageType.Unsubscibe_BodyIndex });
            }
            return this;
        }

        public KinectClient EnableDepthStream(bool enable, int interval = 100) {
            activations[SmartHomeCore.Kinect.FrameType.Depth_data] = enable;
            if (enable) {
                this.client.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.Subscribe_Depth,
                    param = KinectMessageParams.SubscribeDepthParams(interval)
                });
            }
            else {
                this.client.DispatchMessage(new KinectMessage { messageType = KinectMessageType.Unsubscribe_Depth });
            }
            return this;
        }
        public KinectClient EnableIRStream(bool enable, int interval = 100) {
            activations[SmartHomeCore.Kinect.FrameType.ir_data] = enable;
            if (enable) {
                this.client.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.Subscribe_IR,
                    param = KinectMessageParams.SubscribeDepthParams(interval)
                });
            }
            else {
                this.client.DispatchMessage(new KinectMessage { messageType = KinectMessageType.Unsubscribe_IR });
            }
            return this;
        }

        public KinectClient EnablePointCloudStream(bool enable, int interval = 100) {
            activations[SmartHomeCore.Kinect.FrameType.Point_cloud_data] = enable;
            if (enable) {
                this.client.DispatchMessage(new KinectMessage {
                    messageType = KinectMessageType.Subscribe_Point_Cloud,
                    param = KinectMessageParams.SubscribePointCloudParams(interval)
                });
            }
            else {
                this.client.DispatchMessage(new KinectMessage { messageType = KinectMessageType.Unsubscribe_Point_Cloud });
            }
            return this;
        }

        public void ResetBaseTime() {
            this.client.DispatchMessage(KinectMessage.ResetBaseTime());
        }

        private void mainLoop() {
            while (this.isActive) {
                KinectMessage message = this.client.RetrieveMessage<KinectMessage>();
                if (message == null) {
                    this.isActive = false;
                    return;
                }
                lock (latest_data) {
                    latest_data[message.messageType] = message;
                }
                switch (message.messageType) {
                    case KinectMessageType.RGB_data:
                        lock (message) {
                            // Console.WriteLine("RGB_frame received!");
                            FrameArrived(this, new KinectClientFrameArrivedEventArgs {
                                source_interface = this,
                                frameType = FrameType.RGB_Data,
                            });
                            //string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                            //string path = (myPhotos+@"\shots\test" + (this.cs++) + ".jpg");
                            //using (FileStream fs = new FileStream(path, FileMode.Create))
                            //{
                            //    fs.Write(message.payload, 0, message.payload.Length);
                            //}

                        }
                        break;
                    case KinectMessageType.BodyIndex_data:
                        lock (message) {
                            FrameArrived(this, new KinectClientFrameArrivedEventArgs {
                                source_interface = this,
                                frameType = FrameType.BodyIndex_Data,
                            });
                        }
                        break;
                    case KinectMessageType.Depth_data:
                        lock (message) {
                            this.min_depth = message.param.depth_min_reliable;
                            this.max_depth = message.param.depth_max_reliable;
                            FrameArrived(this, new KinectClientFrameArrivedEventArgs {
                                source_interface = this,
                                frameType = FrameType.Depth_data,
                            });
                        }
                        break;
                    case KinectMessageType.IR_data:
                        lock (message) {
                            FrameArrived(this, new KinectClientFrameArrivedEventArgs {
                                source_interface = this,
                                frameType = FrameType.ir_data,
                            });
                        }
                        break;
                    case KinectMessageType.Point_Cloud_Data:
                        lock (message) {
                            FrameArrived(this, new KinectClientFrameArrivedEventArgs {
                                source_interface = this,
                                frameType = FrameType.Point_cloud_data,
                            });
                        }
                        break;
                    case KinectMessageType.Body_data:
                        lock (message) {
                            this.bodies = message.bodies;
                            this.plane = message.floorplane;
                            FrameArrived(this, new KinectClientFrameArrivedEventArgs {
                                source_interface = this,
                                frameType = FrameType.body_data,
                            });
                        }
                        break;
                    case KinectMessageType.Conversion_Tables:
                        lock (message) {
                            this.conversion_table_depth_to_pc = new PointF[this.depthDataFrameSize.Width * this.depthDataFrameSize.Height];
                            this.colorMappedToDepthPoints = new DepthSpacePoint[this.rgbDataFrameSize.Width * this.rgbDataFrameSize.Height];
                            this.depthToColor = new Point2D[this.depthDataFrameSize.Width * this.depthDataFrameSize.Height];
                            byte[] data = KinectProcessing.decompressData(message.payload);
                            BinaryReader reader = new BinaryReader(new MemoryStream(data));
                            for (int i = 0; i < conversion_table_depth_to_pc.Length; i++) {
                                conversion_table_depth_to_pc[i].X = reader.ReadSingle();
                                conversion_table_depth_to_pc[i].Y = reader.ReadSingle();
                            }
                            for (int i = 0; i < colorMappedToDepthPoints.Length; i++) {
                                colorMappedToDepthPoints[i].X = reader.ReadSingle();
                                colorMappedToDepthPoints[i].Y = reader.ReadSingle();
                            }
                            for (int i = 0; i < depthToColor.Length; i++) {
                                depthToColor[i].X = reader.ReadSingle();
                                depthToColor[i].Y = reader.ReadSingle();
                            }
                            reader.Close();
                            Console.WriteLine("Conv Table Received " + data.Length + " from " + message.payload.Length);
                        }
                        break;
                    case KinectMessageType.Bye_Kinect:
                        this.isActive = false;
                        break;
                    default:
                        break;
                }
                message = null;
                Thread.Sleep(1);
            }
        }

        public void Stop() {
            if (this.isActive) {
                this.client.DispatchMessage(new KinectMessage { messageType = KinectMessageType.Bye_Kinect });
                this.isActive = false;
                this.client.Stop();
            }
        }

        public void convertDepthToPointcloud(byte[] depth_data_binary, ushort minReliable, ushort maxReliable,
                                      BufferedPointCloud buffered_cloud) {
            int last_index = 0;
            float[,] cloud_buffer = buffered_cloud.buffer;
            int[] depth_idsm = buffered_cloud.depth_ids;
            if (cloud_buffer.LongLength < depth_data_binary.Length / 2 * 3) {
                throw new Exception("Input cloud buffer is not large enough");
            }
            unsafe {
                fixed (byte* buffer = depth_data_binary) {
                    ushort* depth_data = (ushort*)buffer;
                    for (int i = 0; i < depth_data_binary.Length / 2; i++) {
                        if (depth_data[i] >= minReliable && depth_data[i] <= maxReliable) {
                            cloud_buffer[last_index, 0] = (this.conversion_table_depth_to_pc[i].X * (float)depth_data[i]);
                            cloud_buffer[last_index, 1] = (this.conversion_table_depth_to_pc[i].Y * (float)depth_data[i]);
                            cloud_buffer[last_index, 2] = (float)(depth_data[i]);
                            depth_idsm[last_index] = i;
                            last_index++;
                        }
                    }
                }
            }
            buffered_cloud.max_buffer = last_index;
        }

        public event EventHandler<KinectClientFrameArrivedEventArgs> FrameArrived;

        public bool isEnabled(FrameType frameType) {
            return activations[frameType];
        }


        public Point2D[] getDepthToColorMap() {
            return this.depthToColor;
        }


        public byte[,] getColorForDepthIds(int[] depth_idsm, Image<Bgr, byte> image) {
            byte[,] retColors = new byte[depth_idsm.Length, 3];
            for (int i = 0; i < depth_idsm.Length; i++) {
                Point2D dsp = depthToColor[depth_idsm[i]];
                int tX = (int)(dsp.X + 0.5f);
                int tY = (int)(dsp.Y + 0.5f);
                if (tX > 0 && tX < image.Cols && tY > 0 && tY < image.Rows) {
                    retColors[i, 0] = (byte)image[tY, tX].Red;
                    retColors[i, 1] = (byte)image[tY, tX].Green;
                    retColors[i, 2] = (byte)image[tY, tX].Blue;
                }
            }
            return retColors;
        }


        public float[,] getDeflatedPointCloud(out double timestamp) {
            return getCached<float[,]>(KinectMessageType.Point_Cloud_Data, out timestamp, KinectProcessing.deflatePointCloud);
        }

        private T getCached<T>(KinectMessageType targetType, out double timestamp, Func<byte[], T> processor) {
            KinectMessage message;
            Tuple<object, double> cached_value;
            T retValue;
            lock (latest_data) {
                message = latest_data[targetType];
                cached_value = cached_data[targetType];
            }

            if (message == null) {
                timestamp = -1;
                return default(T);
            }
            else {
                if (cached_value.Item1 != null && message.param.time_stamp == cached_value.Item2) {
                    retValue = (T)cached_value.Item1;
                    timestamp = cached_value.Item2;
                    return retValue;
                }
                else {
                    retValue = processor(message.payload);
                    timestamp = message.param.time_stamp;
                    cached_data[targetType] = new Tuple<object, double>(retValue, timestamp);
                    return retValue;
                }
            }
        }


        public BitmapFrame getDecodedImage(out double timestamp) {
            return getCached<BitmapFrame>(KinectMessageType.RGB_data, out timestamp, KinectProcessing.decodeJPG);
        }

        public byte[] getBodyIndexMapping(out double timestamp) {
            return getCached<byte[]>(KinectMessageType.BodyIndex_data, out timestamp, KinectProcessing.decompressData);
        }


        public byte[] getDepthData(out double timestamp) {
            return getCached<byte[]>(KinectMessageType.Depth_data, out timestamp, KinectProcessing.decompressData);
        }


        public byte[] getIRData(out double timestmap) {
            return getCached<byte[]>(KinectMessageType.IR_data, out timestmap, KinectProcessing.decompressData);
        }

        public KinectBody[] getKinectBodies(out double timestamp) {
            if (latest_data[KinectMessageType.Body_data] != null) {
                KinectMessage km = latest_data[KinectMessageType.Body_data];
                timestamp = km.param.time_stamp;
                return km.bodies;
            }
            else {
                timestamp = -1;
                return null;
            }
        }


        public ushort getMinReliable() {
            return (ushort)min_depth;
        }

        public ushort getMaxReliable() {
            return (ushort)max_depth;
        }
    }
}
