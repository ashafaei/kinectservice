﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace KinectService.Kinect {


    public class KinectNode {
        private KinectSensor kinectNode = null;

        public MultiSourceFrameReader kinectMultiSourceFrameReader { get; set; }

        public CoordinateMapper mapper;
        public DepthSpacePoint[] colorMappedToDepthPoints;
        public ColorSpacePoint[] depthMappedToColorSpacePoints;
        public int depthWidth, depthHeight, colorWidth, colorHeight;

        public PointF[] depthTable { get { return this.mapper.GetDepthFrameToCameraSpaceTable(); } }

        public KinectNode() {
            this.kinectNode = KinectSensor.GetDefault();
            this.kinectMultiSourceFrameReader = this.kinectNode.OpenMultiSourceFrameReader(FrameSourceTypes.Color |
                                                                                            FrameSourceTypes.BodyIndex |
                                                                                            FrameSourceTypes.Depth |
                                                                                            FrameSourceTypes.Infrared |
                                                                                            FrameSourceTypes.Body);
            this.mapper = this.kinectNode.CoordinateMapper;
            FrameDescription depthFrameDescription = this.kinectNode.DepthFrameSource.FrameDescription;

            depthWidth = depthFrameDescription.Width;
            depthHeight = depthFrameDescription.Height;

            FrameDescription colorFrameDescription = this.kinectNode.ColorFrameSource.FrameDescription;

            colorWidth = colorFrameDescription.Width;
            colorHeight = colorFrameDescription.Height;

            this.colorMappedToDepthPoints = new DepthSpacePoint[colorWidth * colorHeight];
            this.depthMappedToColorSpacePoints = new ColorSpacePoint[depthWidth * depthHeight];

            this.kinectMultiSourceFrameReader.MultiSourceFrameArrived += kinectMultiSourceFrameReader_MultiSourceFrameArrived;
        }

        private void kinectMultiSourceFrameReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e) {
            MultiSourceFrame msf = e.FrameReference.AcquireFrame();
            if (msf.DepthFrameReference != null) {
                DepthFrame depthFrame = msf.DepthFrameReference.AcquireFrame();
                if (depthFrame == null) {
                    return;
                }
                using (KinectBuffer depthFrameData = depthFrame.LockImageBuffer()) {
                    this.mapper.MapColorFrameToDepthSpaceUsingIntPtr(
                        depthFrameData.UnderlyingBuffer,
                        depthFrameData.Size,
                        this.colorMappedToDepthPoints);
                    this.mapper.MapDepthFrameToColorSpaceUsingIntPtr(
                        depthFrameData.UnderlyingBuffer,
                        depthFrameData.Size,
                        this.depthMappedToColorSpacePoints);
                }

                depthFrame.Dispose();
                depthFrame = null;
            }
        }

        public void Open() {
            this.kinectNode.Open();
        }

        public void Close() {
            this.kinectNode.Close();
        }
    }
}
