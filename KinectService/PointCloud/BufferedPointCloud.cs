﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeCore.PointCloud {
    public class BufferedPointCloud {
        public float[,] buffer = new float[512 * 424, 3];
        public int[] depth_ids = new int[512 * 424];

        public int max_buffer = 0;

        public float[,] activeCloud {
            get {
                if (max_buffer == 0)
                    return null;
                float [,] retVal = new float[max_buffer, 3];
                Array.Copy(buffer, retVal, max_buffer*3);
                return retVal;
            }
        }
        public void copyFromCloud(BufferedPointCloud bufferedCloud) {
            //Array.Copy(cloud, buffer, cloud.LongLength);
            //max_buffer = cloud.GetLength(0);
            //Console.WriteLine("Fix depth_ids!");
            throw new Exception("Not implemented!");
        }

    }
}
